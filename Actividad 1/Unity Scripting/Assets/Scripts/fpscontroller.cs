using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[RequireComponent(typeof(charactermovement))]
[RequireComponent(typeof(mouselook))]
public class fpscontroller : MonoBehaviour
{

    private charactermovement CharacterMovement;
    private mouselook MouseLook;
    
    

    private void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);

        CharacterMovement = GetComponent<charactermovement>();
        MouseLook = GetComponent<mouselook>();
    }

    private void Update()
    {
        movement();
        rotation();
    }


    private void movement()
    {
        float hMovementInput = Input.GetAxisRaw("Horizontal");
        float vMovementInput = Input.GetAxisRaw("Vertical");

        bool jumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

        CharacterMovement.moveCharacter(hMovementInput, vMovementInput, jumpInput, dashInput);
    }


    private void rotation()
    {
        float hRotationInput = Input.GetAxis("Mouse X");
        float vRotationInput = Input.GetAxis("Mouse Y");

        MouseLook.handleRotation(hRotationInput, vRotationInput);



    }




}
